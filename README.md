# Practicas de Template

Practicas de templates para PAW

## Indice

1. Descarga del repostorio
2. Instalación de dependencias
3. De PHP a Smarty

## Descarga

git clone https://tomasdelvechio@gitlab.com/pawunlu/templates.git

## Instalación

Dentro de la carpeta de cada ejercicio, ejecutar:

```
composer install
```

# Troubleshooting

## Primera vez con git

```
git config --global user.name "Usuario"
git config --global user.email "usermail@gmail.com"
```

## Composer desactualizado

```
Warning: This development build of composer is over 60 days old. It is recommended to update it by running "/usr/local/bin/composer self-update" to get the latest version.
```

Ejecutar `composer self-update`
