<?php

class MiVista {
    protected $templateDir = 'templates/';
    protected $vars = [];

    public function __construct($templateDir = null) {
        if ( ! is_null($templateDir) )
            $this->templateDir = $templateDir;
    }

    public function render($templateFile) {
        if ( file_exists( $this->templateDir . $templateFile ) )
            include $this->templateDir . $templateFile ;
        else
            throw new Exception("No existe archivo $templateFile  en el directorio de templates {$this->templateDir}", 1);
    }

    public function __set($name, $value) {
        $this->vars[$name] = $value;
    }

    public function __get($name) {
        return $this->vars[$name];
    }
}
