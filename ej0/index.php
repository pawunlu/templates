<?php

// Controlador de la app
include_once('MiVista.php');

$templates = new MiVista();
$templates->personajes = ['Ned', 'Jon', 'Bran', 'Arya', 'Sansa'];

$templates->render('index.phtml');

