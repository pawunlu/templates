<?php

require_once("vendor/autoload.php");

class BasicSite {

    private $titulo;
    private $web;

    function __construct($titulo) {
        $this->titulo = $titulo ;
        $this->web = new Smarty();
    }

    private function cabecera($titulo) {
        $this->web->assign("titulo", $this->titulo);
    }

    private function cuerpo() {
        $this->web->assign("cuerpo", "Contenido principal");
    }

    private function pie() {
        $this->web->assign("pie", "Pie de pagina\nContinuacion del pie de pagina");
    }

    private function contenido() {

        $personas = array(  ['nombre' => 'Walter', 'apellido' => 'White'],
                            ['nombre' => 'Jesse', 'apellido' => 'Pinkman'],
                            ['nombre' => 'Saul', 'apellido' => 'Goodman'],
                            ['nombre' => 'Gus', 'apellido' => 'Fring'],
                            ['nombre' => 'Jane', 'apellido' => 'Margolis']   );

        $this->web->assign("personas", $personas);
    }

    public function crearWeb() {
        $this->cabecera($this->titulo);
        $this->cuerpo();
        $this->contenido();
        $this->pie();
        $this->web->display("ej1-smarty.tpl");
    }
}

$pagina = new BasicSite("Clase de Templates - PAW 2017");
$pagina->crearWeb();
