{* plantilla smarty *}
<html>
    <head>
        <title>{$titulo|upper}</title>
    </head>
    <body>
        {$cuerpo|truncate:10:"..."}

        <table>
            {foreach $personas as $persona}
                <tr>
                    <td>{$persona.nombre}</td>
                    <td>{$persona.apellido}</td>
                <tr>
            {/foreach}
        </table>

        {$pie|nl2br}
    </body>
</html>
