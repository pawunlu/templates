<?php

class BasicSite {

    private $titulo;

    function __construct($titulo) {
        $this->titulo = $titulo;
    }

    private function cabecera($titulo) {
        echo "<html>";
        echo "<head>";
        echo "<title>$titulo</title>";
        echo "</head>";
        echo "<body>";
    }

    private function cuerpo() {
        echo "Contenido principal";
    }

    private function pie() {
        echo "Pie de pagina";
        echo "</body>";
        echo "</html>";
    }

    public function crearWeb() {
        $this->cabecera($this->titulo);
        $this->cuerpo();
        $this->pie();
    }
}

$pagina = new BasicSite("Clase de Templates - PAW 2017");
$pagina->crearWeb();
